#!/usr/bin/env bash

if [ ! -f "$1" ]; then
    1>&2 echo "Usage: $0 <backup>.tar.gz"
    exit 1
fi

if [ -d "./volumes" ]; then
   1>&2 echo "Will not overwrite volumes directory. Remove it first"
   exit 1
fi

tar -xvf "$1"
